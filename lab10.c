#include<stdio.h>
int main()
{
	FILE *fp;
	char ch;
	printf("Enter the data\n");
	fp=fopen("input.txt","w");
	while((ch=getchar())!=EOF)
	{
		putc(ch,fp);
	}
	fclose(fp);
	printf("Stored input\n");
	fp=fopen("input.txt","r");
	while((ch=getc(fp))!=EOF)
	{
		printf("%c",ch);
	}
	fclose(fp);
	return 0;
	
}