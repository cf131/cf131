#include<stdio.h>
void art(int *a, int *b)
{
	int add=*a + *b;
	int sub=*a - *b;
	int prod=*a * *b;
	float div=*a / *b;
	int rem=*a % *b;
	printf("Sum of %d and %d = %d\n",*a,*b,add);
	printf("Difference of %d and %d = %d\n",*a,*b,sub);
	printf("Product of %d and %d = %d\n",*a,*b,prod);
	printf("Quotient of %d and %d = %f\n",*a,*b,div);
	printf("Remainder of %d and %d = %d\n",*a,*b,rem);
}
int main()
{
	int a,b;
	printf("Enter two integers\n");
	scanf("%d%d",&a,&b);
	art(&a,&b);
	return 0;
}	
	