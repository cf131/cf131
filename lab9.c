#include<stdio.h>
void swap(int *x,int *y)
{
	int temp;
	temp=*x;
	*x=*y;
	*y=temp;
}
int main()
{
	int a,b;
	printf("Enter two integers\n");
	scanf("%d%d",&a,&b);
	printf("Before Swapping\na=%d \nb=%d \n",a,b);
	swap(&a,&b);
	printf("After Swapping\na=%d \nb=%d \n",a,b);
	return 0;
}	